# Week 7 Mini Project

## Project description:

This project focuses on the utilization of a Vector Database for advanced data processing capabilities, specifically leveraging the Qdrant service for efficient data management and retrieval. Our objective was to demonstrate the complete lifecycle of data within a vector database, including the ingestion of data, execution of sophisticated queries and aggregations, and the visualization of query outputs. Through the development of a custom Rust application, we aimed to showcase the power and flexibility of vector databases in handling complex data structures and queries, thus providing insights and facilitating data-driven decisions in real-time.

## Procedure:

1. Create a new Rust project with the following command: 
cargo new mini7
2. Modify the Cargo.toml file by adding the dependencies.
[dependencies]
qdrant-client = "1.8.0"
tokio = { version = "1.36.0", features = ["rt-multi-thread"] }
serde_json = "1.0.114"
tonic = "0.11.0"
anyhow = "1.0.81"
3. Make sure that you have install the docker correctly
4. Use docker to pull the Qdrant image:
docker pull qdrant/qdrant
5.run the Qdrant service:
docker run -p 6333:6333 -p 6334:6334 \
    -e QDRANT__SERVICE__GRPC_PORT="6334" \
qdrant/qdrant

Ingest data into Vector database:
1. initialize_qdrant_client():
This function establishes the foundational step of our data ingestion process by initializing a connection to the Qdrant Vector Database. Through a carefully constructed configuration, it targets the Qdrant server's URL, ensuring seamless communication between our application and the database. 
2. create_collection():
Upon successfully connecting to the Qdrant server, our method proceeds with the creation of a dedicated collection named "movies_collection". This function first checks for the existence of the collection, deleting it if present, to ensure a clean slate. It then creates a new collection, configuring it with specific vector parameters such as size and distance metric, tailored to optimize the storage and retrieval of our movie vector data.
3. ingest_movies_vectors():
The final step in our data ingestion strategy involves populating the "movies_collection" with vector data representing movies. We craft eight unique vectors, each corresponding to a popular movie, encapsulating diverse attributes such as genre, directorial style, and critical reception into a four-dimensional space. These vectors, along with their associated payloads containing movie titles, are then upserted into the collection, ensuring each movie is accurately represented in our vector database for subsequent querying and analysis.

## Perform queries and aggregations

1.  query_vectors():
In the following I would test our vector database with a given query vector[0.3, 0.4, 0.4, 0.7], it could return 3 most closest match result:
```javascript
async fn query_vectors(client: &QdrantClient, collection_name: &str) -> Result<()> {
    // Run a query on the data
    let search_result = client
        .search_points(&SearchPoints {
            collection_name: collection_name.into(),
            vector: vec![0.3, 0.4, 0.4, 0.7],
            limit: 3,
            with_payload: Some(true.into()),
            ..Default::default()
        })
        .await
        .unwrap();

    // Print header
    println!("Search Results:");

    // Iterate over each found point
    for (index, point) in search_result.result.iter().enumerate() {
        // Format the payload nicely
        let formatted_payload = serde_json::to_string_pretty(&point.payload)
            .unwrap_or_else(|_| "Unable to format payload".to_string());

        // Print each point's information
        println!("Point {}: ", index + 1);
        println!(" - Payload: {}", formatted_payload);
        println!(" - Score: {}", point.score); // Print the score instead of distance
        println!(); // Add an empty line for better readability
    }

    Ok(())
}
```
2. query_vectors_with_filter():
I ran this function to checks if the previous query result has a title name of "The Godfather":
```javascript
async fn query_vectors_with_filter(client: &QdrantClient, collection_name: &str) -> Result<()> {
    // Run a query on the data with a filter for "The Godfather"
    let search_result = client
        .search_points(&SearchPoints {
            collection_name: collection_name.into(),
            vector: vec![0.3, 0.4, 0.4, 0.7],
            filter: Some(Filter::all([Condition::matches(
                "title",
                "The Godfather".to_string(),
            )])),
            limit: 3,
            with_payload: Some(true.into()),
            ..Default::default()
        })
        .await
        .unwrap();

    // Print header
    println!("Search Results After Filter:");

    // Iterate over each found point
    for (index, point) in search_result.result.iter().enumerate() {
        // Format the payload nicely
        let formatted_payload = serde_json::to_string_pretty(&point.payload)
            .unwrap_or_else(|_| "Unable to format payload".to_string());

        // Print each point's information
        println!("Point {}: ", index + 1);
        println!(" - Payload: {}", formatted_payload);
        println!(" - Score: {}", point.score); // Print the score instead of distance
        println!(); // Add an empty line for better readability
    }

    Ok(())
}
```

## Visualizations:

### Qdrant logs:
![Alt text](image.png)

### Result of query_vectors():
![Alt text](image-1.png)

### Result of query_vectors_with_filter():
![Alt text](image-2.png)
 
